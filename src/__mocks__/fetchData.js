const users = [
    {
        venueName: "some name",
        formattedAddress: ["some address"],
        venueCategories: ["some categories"]
    }
];

export default function request(url) {
    return new Promise((resolve, reject) => {
        const userID = parseInt(url.substr("/users/".length), 10);
        process.nextTick(
            () =>
                users[userID]
                    ? resolve(users[userID])
                    : reject({
                          error: "User with " + userID + " not found."
                      })
        );
    });
}
