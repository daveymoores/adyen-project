class GetFourSquareData {
    fetchData(loc) {
        // fetch all the data
        const promise = new Promise((resolve, reject) => {
            fetch(
                `https://api.foursquare.com/v2/venues/explore?client_id=KKYG0HLNYTUAIXX54LJFDRWAPN51EP0NPHLZOY2AEXMMOHB1&client_secret=D0Q0AUAPJS4LAA33HUMPUMH4ZFGR0DFTSQK4UBSHI0ZWTONB&ll=${
                    loc.latitude
                },${loc.longitude}&v=20181207`
            ).then(res => {
                res.json().then(res => {
                    //sort the data then return
                    this.returnRelevantData(res).then(results => {
                        resolve(results);
                    });
                });
            });
        });

        return promise;
    }

    returnRelevantData(results) {
        const venueWrapper = [];

        // get the data into a more usable format
        const promise = new Promise((resolve, reject) => {
            //map through results returned from fetch
            results.response.groups[0].items.map(item => {
                // create an empty object for venues
                let venue = {
                    venueName: "",
                    venueCategories: "",
                    formattedAddress: ""
                };

                // assign values, add them to the array and return it
                venue.venueName = item.venue.name;
                venue.venueCategories = item.venue.categories;
                venue.formattedAddress = item.venue.location.formattedAddress;
                return venueWrapper.push(venue);
            });

            return resolve(venueWrapper);
        });

        return promise;
    }
}

export default GetFourSquareData;
