import React from "react";
import styled from "styled-components";

const LocationLabel = styled.label`
    font-size: 0px;
`;

const LocationInput = styled.input`
    background-color: #355de4;
    padding: 10px 15px;
    border-radius: 5px;
    font-size: 18px;
    color: white;
    border: none;
    margin: 10px;
`;

const ShowLocation = props => {
    const { locString } = props;
    return (
        <LocationLabel>
            Longitude
            <LocationInput type="text" value={locString.toFixed(6)} readOnly />
        </LocationLabel>
    );
};

export default ShowLocation;
