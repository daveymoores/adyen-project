import React, { Component } from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import LoadingContainer from "./LoadingContainer";

const style = {
    width: "100%",
    height: "100%"
};

export class MapContainer extends Component {
    constructor(props) {
        super(props);

        this.centerMoved = this.centerMoved.bind(this);
        this.callback = this.callback.bind(this);
        this.state = {
            loc: {
                longitude: 0,
                latitude: 0
            },
            loading: true
        };
    }

    componentDidMount(props) {
        // on page load get the current use location and setState
        navigator.geolocation.getCurrentPosition(
            position => {
                const { latitude, longitude } = position.coords;

                this.setState({
                    loc: { longitude: longitude, latitude: latitude },
                    loading: false
                });
            },
            () => {
                this.setState({ loading: false });
            }
        );
    }

    componentDidUpdate(prevProps, prevState) {
        // when the state updates send the location through to app
        const { loc } = this.state;
        if (loc !== prevState.loc) {
            this.callback();
        }
    }

    callback() {
        const { loc } = this.state;
        this.props.callback(loc);
    }

    centerMoved(mapProps, map) {
        // after map is dragged return location and set the state
        this.setState({
            loc: {
                longitude: map.getCenter().lng(),
                latitude: map.getCenter().lat()
            }
        });
    }

    render() {
        const { loading, loc } = this.state;
        const { google } = this.props;

        if (loading) {
            return null;
        }

        // return the map component and a marker that is updated on dragend
        return (
            <Map
                onDragend={this.centerMoved}
                zoom={15}
                google={google}
                style={style}
                initialCenter={{
                    lat: loc.latitude,
                    lng: loc.longitude
                }}
            >
                <Marker position={{ lat: loc.latitude, lng: loc.longitude }} />
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: "AIzaSyCLeEYuObY-VHbxeekg9L9FO0TnPnUDFaU",
    LoadingContainer: LoadingContainer
})(MapContainer);
