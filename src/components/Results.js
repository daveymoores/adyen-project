import React, { Component } from "react";
import styled from "styled-components";
import ShowLocation from "./ShowLocation";
import SuggestedVenue from "./SuggestedVenue";
import LoadingContainer from "./LoadingContainer";
import GetFourSquareData from "../getFourSquareData";

const LLContainer = styled.div`
    width: 100%;
`;

const Title = styled.h1`
    font-size: 32px;
`;

Title.displayName = "Title";

const LoadingWrapper = styled.div`
    position: relative;
`;

class Results extends Component {
    constructor(props) {
        super(props);

        this.getFourSquareData = new GetFourSquareData();
        this.getKey = this.getKey.bind(this);
        this.counter = 0;
        this.state = {
            results: null
        };
    }

    getKey() {
        return this.counter++;
    }

    componentDidMount() {
        this.setState({ lifecycle: "mounted" });
    }

    componentDidUpdate(prevProps) {
        const { loc } = this.props;
        if (loc !== prevProps.loc) {
            this.getFourSquareData.fetchData(loc).then(res => {
                this.setState({ results: res });
            });
        }
    }

    handleClick = () => {
        return (this.titleRef.innerText = "Tomato");
    };

    render() {
        const { results } = this.state;
        const { loc } = this.props;

        if (results) {
            return (
                <React.Fragment>
                    <p className="lifecycle">{this.state.lifecycle}</p>
                    <button onClick={this.handleClick}>Change Text</button>
                    <Title ref={el => (this.titleRef = el)}>
                        FourSquare Venue Finder
                    </Title>
                    <p>Drag map to choose a location and update results</p>
                    <LLContainer>
                        <ShowLocation locString={loc.longitude} />
                        <ShowLocation locString={loc.latitude} />

                        <h2>Suggested Venues</h2>
                        {results.map(venue => (
                            <SuggestedVenue
                                key={this.getKey()}
                                name={venue.venueName}
                                address={venue.formattedAddress}
                                categories={venue.venueCategories}
                            />
                        ))}
                    </LLContainer>
                </React.Fragment>
            );
        }
        return (
            <React.Fragment>
                <p className="lifecycle">{this.state.lifecycle}</p>
                <button onClick={this.handleClick}>Change Text</button>
                <Title ref={el => (this.titleRef = el)}>
                    FourSquare Venue Finder
                </Title>
                <LLContainer>
                    <ShowLocation locString={loc.longitude} />
                    <ShowLocation locString={loc.latitude} />

                    <h2>Suggested Venues</h2>
                    <LoadingWrapper>
                        <LoadingContainer />
                    </LoadingWrapper>
                </LLContainer>
            </React.Fragment>
        );
    }
}

export default Results;
