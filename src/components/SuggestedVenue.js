import React from "react";
import styled from "styled-components";

const StyledCategories = styled.ul`
    padding: 0;
    margin: 0 0 20px;
    list-style: none;
    display: flex;
    justify-content: center;

    li {
        background-color: #92e88f;
        padding: 5px 8px;
        border-radius: 3px;
    }
`;

const StyledAddress = styled.ul`
    padding: 0;
    margin: 0 0 60px;
    list-style: none;
`;

const Subtitle = styled.h4`
    text-transform: uppercase;
    font-size: 10px;
    font-weight: bold;
`;

const SuggestedVenue = props => {
    const { name, address, categories } = props;
    let counter = 0;

    function updateCounter(count) {
        return counter++;
    }

    function returnCats(cat) {
        return cat.map(x => (
            <li key={`cat-${updateCounter(counter)}`}>{x.shortName}</li>
        ));
    }

    function returnAdd(add) {
        return add.map(x => <li key={`add-${updateCounter(counter)}`}>{x}</li>);
    }

    return (
        <div>
            <h3>{name}</h3>
            <Subtitle>Categories</Subtitle>
            <StyledCategories>{returnCats(categories)}</StyledCategories>
            <Subtitle>Address</Subtitle>
            <StyledAddress>{returnAdd(address)}</StyledAddress>
        </div>
    );
};

export default SuggestedVenue;
