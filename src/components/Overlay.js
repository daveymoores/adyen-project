import React, { Component, Fragment } from "react";
import styled from "styled-components";

const OverlayBg = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.8);
	z-index: 2;
	display: ${props => (props.visible ? `block` : `none`)};
`;

export default class Overlay extends Component {
	state = { visible: this.props.initialVisibility };

	static defaultProps = {
		initialVisibility: false
	};

	handleClick = () => {
		this.setState(({ visible }) => ({ visible: !visible }));
	};

	render() {
		const { visible } = this.state;

		return (
			<Fragment>
				<OverlayBg visible={visible} />
				{this.props.render({
					...this.state,
					onClick: this.handleClick
				})}
			</Fragment>
		);
	}
}
