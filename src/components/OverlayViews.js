import React, { Component } from "react";
import styled from "styled-components";

const OverlayContainer = styled.div`
	background-color: white;
	width: auto;
	height: auto;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate3d(-50%, -50%, 0);
	border-radius: 4px;
	box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.3);
	z-index: 3;
	display: ${props => (props.visible ? `block` : `none`)};
`;

const OverlayHeader = styled.header`
	width: auto;
	padding: 20px;
	position: relative;
`;

const Title = styled.h3`
	font-size: 24px;
	text-align: center;
	margin: 0;
`;

const OverlayContent = styled.div`
	width: auto;
	padding: 20px;
`;

const CloseBtn = styled.button`
	transform: rotate(45deg);
	font-weight: 100;
	position: absolute;
	right: 0;
	top: 0;
	border: none;
	background-color: transparent;
	font-size: 50px;
	outline: none;
	cursor: pointer;
`;

export class OverlayViewOne extends Component {
	render() {
		const { visible, onClick } = this.props;

		return (
			<OverlayContainer visible={visible}>
				<OverlayHeader>
					<CloseBtn onClick={onClick}>+</CloseBtn>
					<Title>This is a Title</Title>
				</OverlayHeader>
				<OverlayContent>
					<p>
						Irure adipisicing pariatur duis laborum commodo aliqua
						do ea. Aliquip exercitation veniam nisi ut. Deserunt
						irure cupidatat velit mollit cillum quis quis amet
						tempor ad ipsum amet fugiat.
					</p>
				</OverlayContent>
			</OverlayContainer>
		);
	}
}

export class OverlayViewTwo extends Component {
	render() {
		const { visible, onClick } = this.props;

		return (
			<OverlayContainer visible={visible}>
				<OverlayHeader>
					<CloseBtn onClick={onClick}>+</CloseBtn>
					<Title>Modal title</Title>
				</OverlayHeader>
				<OverlayContent>
					<p>Different content</p>
				</OverlayContent>
			</OverlayContainer>
		);
	}
}
