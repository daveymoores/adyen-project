import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import ShowLocation from "../ShowLocation";

configure({ adapter: new Adapter() });

describe("<ShowLocation />", () => {
    const mockProps = {
        locString: 45.56887848738
    };

    it("doesn't crash", () => {
        const wrapper = shallow(<ShowLocation {...mockProps} />);
        expect(wrapper).toMatchSnapshot();
    });
});
