import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import SuggestedVenue from "../SuggestedVenue";
import toJson from "enzyme-to-json";

configure({ adapter: new Adapter() });

describe("<SuggestedVenue />", () => {
    const fakeProps = {
        name: "fake name",
        address: ["my house", "my street", "my city"],
        categories: [{ shortName: "tomato" }]
    };

    it("receives props", () => {
        const tree = shallow(<SuggestedVenue {...fakeProps} />);
        expect(toJson(tree)).toMatchSnapshot();
    });

    it("renders categories correctly", () => {
        const tree = shallow(<SuggestedVenue {...fakeProps} />);
        expect(tree.find("h3").text()).toBe("fake name");
    });

    it("renders the correct number of li elements", () => {
        const tree = shallow(<SuggestedVenue {...fakeProps} />);
        expect(tree.find("li").length).toBe(4);
    });
});
