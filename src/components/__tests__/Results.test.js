import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import GetFourSquareData from "../../getFourSquareData";
import Results from "../Results";

configure({ adapter: new Adapter() });

jest.mock("../../getFourSquareData", () =>
    jest.fn().mockImplementation(() => ({
        fetchData: jest.fn()
    }))
);

describe("<Results />", () => {
    const mockProps = {
        loc: {
            longitude: 99.9999999,
            latitude: 99.9999999
        },
        results: [
            {
                venueName: "some name",
                formattedAddress: ["some address"],
                venueCategories: ["some categories"]
            }
        ]
    };

    it("calls new on GetFourSquareData", () => {
        const getFourSquareData = new GetFourSquareData(mockProps.loc);
        const spy = jest.spyOn(getFourSquareData, "fetchData");
        getFourSquareData.fetchData();
        expect(spy).toBeCalled();
        spy.mockRestore();
    });

    it("doesn't crash", () => {
        const wrapper = shallow(<Results {...mockProps} />);
        expect(wrapper).toMatchSnapshot();
    });

    it("Calls ComponentDidMount, updates p tag text", () => {
        const mountSpy = jest.spyOn(Results.prototype, "componentDidMount");
        const updateSpy = jest.spyOn(Results.prototype, "componentDidUpdate");
        const tree = shallow(<Results {...mockProps} />);
        expect(mountSpy.mock.calls.length).toBe(1);
        expect(tree.find(".lifecycle").text()).toBe("mounted");
        tree.setProps({ loc: mockProps.loc });
        expect(updateSpy.mock.calls.length).toBe(2);
    });

    it("button click changes p text", () => {
        const tree = shallow(<Results {...mockProps} />);
        const title = tree.find("Title");
        const button = tree.find("button");
        expect(title.text()).toBe("FourSquare Venue Finder");
        button.simulate("click");
        // tree.instance().handleClick();
        // expect(title.text()).toBe("Tomato");
    });
});
