import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import LoadingContainer from "../LoadingContainer";

configure({ adapter: new Adapter() });

describe("<LoadingContainer />", () => {
    it("doesn't crash", () => {
        const wrapper = shallow(<LoadingContainer />);
        expect(wrapper).toMatchSnapshot();
    });
});
