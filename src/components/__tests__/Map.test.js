import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Map from "../Map";

configure({ adapter: new Adapter() });

describe("<Map />", () => {
    it("doesn't crash", () => {
        const wrapper = shallow(<Map />);
        expect(wrapper).toMatchSnapshot();
    });
});
