import React, { Component, Fragment } from "react";
import styled from "styled-components";

const OverlayBg = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.8);
	z-index: 2;
	display: ${props => (props.visible ? `block` : `none`)};
`;

const OverlayContainer = styled.div`
	background-color: white;
	width: auto;
	height: auto;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate3d(-50%, -50%, 0);
	border-radius: 4px;
	box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.3);
	z-index: 3;
	display: ${props => (props.visible ? `block` : `none`)};
`;

const OverlayHeader = styled.header`
	width: auto;
	padding: 20px;
	position: relative;
`;

const Title = styled.h3`
	font-size: 24px;
	text-align: center;
	margin: 0;
`;

const OverlayContent = styled.div`
	width: auto;
	padding: 20px;
`;

const CloseBtn = styled.button`
	transform: rotate(45deg);
	font-weight: 100;
	position: absolute;
	right: 0;
	top: 0;
	border: none;
	background-color: transparent;
	font-size: 50px;
	outline: none;
	cursor: pointer;
`;

export const withOverlay = buttons => {
	return class Overlay extends Component {
		state = { visible: this.props.initialVisibility };

		counter = 0;

		static defaultProps = {
			initialVisibility: false
		};

		handleClick = () => {
			this.setState(({ visible }) => ({ visible: !visible }));
		};

		getKey = () => {
			return this.counter++;
		};

		render() {
			const { visible } = this.state;

			return (
				<Fragment>
					<OverlayBg visible={visible} />
					<OverlayContainer visible={visible}>
						<OverlayHeader>
							<CloseBtn onClick={this.handleClick}>+</CloseBtn>
							<Title>This is a Title</Title>
						</OverlayHeader>
						<OverlayContent>
							<p>
								Irure adipisicing pariatur duis laborum commodo
								aliqua do ea. Aliquip exercitation veniam nisi
								ut. Deserunt irure cupidatat velit mollit cillum
								quis quis amet tempor ad ipsum amet fugiat.
							</p>
						</OverlayContent>
					</OverlayContainer>

					{buttons.map(ButtonComponent => {
						return (
							<ButtonComponent
								key={this.getKey()}
								onClick={this.handleClick}
							/>
						);
					})}
				</Fragment>
			);
		}
	};
};
