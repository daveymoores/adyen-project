// @flow

import React from "react";
import { Pulse } from "styled-spinkit";
import styled from "styled-components";

const StyledLoader = styled(Pulse)`
    position: absolute;
    top: 25%;
    left: 50%;
    transform: translate3d(50%, 50%, 0);
`;

const LoadingContainer = props => <StyledLoader color={"#2EC272"} />;

export default LoadingContainer;
