import React, { Component } from "react";
import styled from "styled-components";
import MapContainer from "./components/Map";
import Results from "./components/Results";
// import Overlay from "./components/Overlay";
// import { OverlayViewTwo } from "./components/OverlayViews";
import { withOverlay } from "./components/HocOverlay";
import "./App.css";

const Container = styled.div`
	position: relative;
	width: 100vw;
	height: 100vh;
	display: flex;
	justify-content: center;
`;

const ResultsWrapper = styled.div`
	width: 50%;
	height: 100vh;
	background-color: white;
	padding: 40px;
	position: relative;
	overflow: scroll;
`;

const MapWrapper = styled.div`
	width: 50%;
	height: 100%;
	position: relative;
`;

const Button = ({ onClick, color }) => (
	<button onClick={onClick}>{color}</button>
);

class App extends Component {
	constructor() {
		super();

		this.setLocation = this.setLocation.bind(this);
		this.state = {
			loc: {
				longitude: 0,
				latitude: 0
			},
			fetching: false
		};
	}

	setLocation(loc) {
		// callback function that takes the location from the map component
		const { longitude, latitude } = loc;
		this.setState({
			loc: {
				longitude,
				latitude
			}
		});
	}

	render() {
		const { fetching, loc } = this.state;
		const arr = [];
		const btns = {
			options: ["red", "blue", "green", "yellow"]
		};
		btns.options.map(color => {
			return arr.push(Button({ color: color }));
		});
		const Hoc = withOverlay(arr);

		return (
			<div className='App'>
				{/*<Overlay render={props => <OverlayViewTwo {...props} />} />*/}
				<Hoc />
				<Container>
					<MapWrapper>
						<MapContainer callback={this.setLocation} />
					</MapWrapper>
					<ResultsWrapper>
						<Results loc={loc} fetching={fetching} />
					</ResultsWrapper>
				</Container>
			</div>
		);
	}
}

export default App;
