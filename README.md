This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Adyen project

Free hosting with Heroku so there is a small amount of boot-up time on first loading

Uses google maps for setting the location on dragend event

## Available Scripts

In the project directory, you can run:

### `npm start`

### `npm test`

### `npm run build`
